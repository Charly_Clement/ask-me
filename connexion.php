<?php

session_start();

include 'header.php';

if ( isset($_POST['email']) && !empty($_POST['email'])
  && isset($_POST['mdp']) && !empty($_POST['mdp'])) {
// récupération de l'utilisateur avec son adresse email
$utilisateur = $pdo->prepare("SELECT * FROM utilisateur WHERE `email` = :email");
$utilisateur->execute(['email' => $_POST['email']]);
$utilisateur = $utilisateur->fetch();

// vérification du mot de passe
if ($utilisateur['mdp'] === ($_POST['mdp'])) {
    $_SESSION['connecte'] = $utilisateur['id_utilisateur'];
    header("Location: ask-me/index.php");
}
}



?>


<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <p><b>Connectez vous</b></p>
  </div>

  <div class="card">
    <div class="card-body register-card-body">

      <?php echo $erreur; ?>

        <form method="post">
            <div class="input-group mb-3">
                <input type="email" name="email" class="form-control" placeholder="Email">
                <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-envelope"></span>
                </div>
                </div>
            </div>
            <div class="input-group mb-3">
                <input type="password" name="mdp" class="form-control" placeholder="mot de passe">
                <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-lock"></span>
                </div>
                </div>
            </div>
            <div class="row">
                <div class="col-5 offset-7">
                <button type="submit" name="submit" class="btn btn-primary btn-block">Se connecter</button>
                </div>
            </div>
        </form>
    </div>
    <!-- /.form-box -->
  </div><!-- /.card -->
</div>
<!-- /.register-box -->

<?php include 'footer.php'; ?>

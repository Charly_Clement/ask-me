<?php
	include '../connexion-bdd.php';
	$id_article = $_GET['id'];

	$nom         = isset($_POST['nom'])         && !empty($_POST['nom'])         ? $_POST['nom']          : '';
	$commentaire = isset($_POST['commentaire']) && !empty($_POST['commentaire']) ? $_POST['commentaire']  : '';
	$submit      = isset($_POST['submit'])      && !empty($_POST['submit'])      ? $_POST['submit']       : '';

	$request = $pdo->prepare("SELECT * FROM article WHERE id_article=?");
	$request->execute([$id_article]);
	$request = $request->fetchAll();
	$article = $request[0];
	
	$request = $pdo->prepare("SELECT * FROM commentaire WHERE id_article=? ORDER BY date_commentaire ASC");
	$request->execute([$id_article]);
	$nb = $request -> rowcount();
	
	if ($submit) {
		$message = $pdo->prepare("INSERT INTO commentaire (id_article,nom,commentaire,date_commentaire)
										VALUES (?,?,?,now())");
		$message->execute([$id_article,$nom,$commentaire]);
		header('Location: index.php');
	}

?>
<!DOCTYPE html>
<html lang="fr">

<head>
	<meta charset="utf-8">
	<title>ASK ME</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!-- Main Style -->
	<link rel="stylesheet" href="style.css">
	<!-- Skins -->
	<link rel="stylesheet" href="css/skins/skins.css">
	<!-- Responsive Style -->
	<link rel="stylesheet" href="css/responsive.css">
	<!-- Dark Style -->
	<link rel="stylesheet" href="css/dark.css">
	<!-- Favicons -->
	<link rel="shortcut icon" href="images/favicon.png">
</head>

<body>

	<div class="breadcrumbs">
		<section class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>Articles</h1>
				</div>
				<div class="col-md-12">
					<div class="crumbs">
						<a href="index.php">Home</a>
					</div>
				</div>
			</div><!-- End row -->
		</section><!-- End container -->
	</div><!-- End breadcrumbs -->
	
	<section class="container main-content">
		<div class="row">
			<div class="col-md-12">

				<article class="post clearfix">
					<div class="post-inner">
						<div class="post-img"><img src="<?php echo $article['photo'] ?>" alt=""></a></div>
						<h2 class="post-title"><span class="post-type"><i class="icon-picture"></i></span><a href="single_post.html"><?php echo $article['titre'] ?></a></h2>
						<div class="post-meta">
							<span class="meta-date"><i class="icon-time"></i><?php echo $article['date_parution'] ?></span>
						</div>
						<div class="post-content">
							<p><?php echo $article['descrip'] ?></p>
						</div>
					</div>
				</article>

				<div class="page-content">
					<h2>Laissez votre commentaire !</h2>
					<form action="" method="post" class="form-style form-style-3 form-style-5">
						<div class="form-inputs clearfix">
							<p>
								<label for="nom" class="required">nom<span>*</span></label>
								<input type="text" class="required-item" value="" name="nom" id="nom" aria-required="true">
							</p>
						</div>
						<div class="form-textarea">
							<p>
								<label for="message" class="required">Message<span>*</span></label>
								<textarea id="message" class="required-item" name="commentaire" aria-required="true" cols="58" rows="7"></textarea>
							</p>
						</div>
						<p class="form-submit">
							<input type="submit" name="submit" value="Envoyer" class="submit button small color">
						</p>
					</form>
				</div><br><br>

				<div id="commentlist" class="page-content">
					<div class="boxedtitle page-title"><h2> ( <span class="color"><?php echo $nb ?></span> ) Commentaire<?php echo $nb > 0 ? 's' : '' ?></h2></div>

					<?php
						$req = $pdo->prepare("SELECT * FROM commentaire WHERE id_article = ? ORDER BY date_commentaire"); 
						$req->execute([$id_article]);
						$results = $req->fetchAll();

						foreach($results as $commentaire) { ?>
						<ol class="commentlist clearfix">
							<li class="comment">
								<div class="comment-body clearfix"> 
									<div class="comment-text">
										<h3>De : <?php echo $commentaire['nom'] ?></h3>
										<h5>Le : <?php echo $commentaire['date_commentaire'] ?></h5>
										<div class="text"><p><?php echo $commentaire['commentaire'] ?></p></div>
									</div>
								</div>
							</li>
						</ol><!-- End commentlist -->
						<hr><br>
					<?php } ?>

				</div><!-- End page-content -->


			</div><!-- End main -->
		</div><!-- End row -->
	</section><!-- End container -->
	
	<footer id="footer-bottom">
		<section class="container">
			<div class="copyrights f_left">Copyright 2020 Enssop | <a href="#">d3V w3b</a></div>
			<div class="social_icons f_right">
				<ul>
					<li class="twitter"><a original-title="Twitter" class="tooltip-n" href="#"><i class="social_icon-twitter font17"></i></a></li>
					<li class="facebook"><a original-title="Facebook" class="tooltip-n" href="#"><i class="social_icon-facebook font17"></i></a></li>
					<li class="gplus"><a original-title="Google plus" class="tooltip-n" href="#"><i class="social_icon-gplus font17"></i></a></li>
					<li class="youtube"><a original-title="Youtube" class="tooltip-n" href="#"><i class="social_icon-youtube font17"></i></a></li>
					<li class="skype"><a original-title="Skype" class="tooltip-n" href="skype:#?call"><i class="social_icon-skype font17"></i></a></li>
					<li class="flickr"><a original-title="Flickr" class="tooltip-n" href="#"><i class="social_icon-flickr font17"></i></a></li>
					<li class="rss"><a original-title="Rss" class="tooltip-n" href="#"><i class="social_icon-rss font17"></i></a></li>
				</ul>
			</div><!-- End social_icons -->
		</section><!-- End container -->
	</footer><!-- End footer-bottom -->
</div><!-- End wrap -->

<div class="go-up"><i class="icon-chevron-up"></i></div>

<!-- js -->
<script src="js/jquery.min.js"></script>
<script src="js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="js/jquery.easing.1.3.min.js"></script>
<script src="js/html5.js"></script>
<script src="js/twitter/jquery.tweet.js"></script>
<script src="js/jflickrfeed.min.js"></script>
<script src="js/jquery.inview.min.js"></script>
<script src="js/jquery.tipsy.js"></script>
<script src="js/tabs.js"></script>
<script src="js/jquery.flexslider.js"></script>
<script src="js/jquery.prettyPhoto.js"></script>
<script src="js/jquery.carouFredSel-6.2.1-packed.js"></script>
<script src="js/jquery.scrollTo.js"></script>
<script src="js/jquery.nav.js"></script>
<script src="js/tags.js"></script>
<script src="js/jquery.bxslider.min.js"></script>
<script src="js/custom.js"></script>
<!-- End js -->

</body>
</html>
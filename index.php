
<?php 

    require 'header.php';
    require 'connexion-bdd.php';

    $erreur = null;

    $prenom = isset($_POST['prenom']) && !empty($_POST['prenom']) ? $_POST['prenom'] : '';
    $email  = isset($_POST['email'])  && !empty($_POST['email'])  ? $_POST['email']  : '';
    $mdp    = isset($_POST['mdp'])    && !empty($_POST['mdp'])    ? $_POST['mdp']    : '';

    if($submit = isset($_POST['submit'])) {
        if ($prenom) {
            if ($email) {
                if ($mdp) {
                    //$mdp = password_hash($mdp, PASSWORD_DEFAULT);
                    $request = $pdo->prepare("INSERT INTO utilisateur (prenom,email,mdp,date_inscription)
                                                VALUES (:prenom,:email,:mdp,now())");
                    $request->execute(['prenom'=>$prenom,'email'=>$email,'mdp'=>$mdp]);
                    header('Location: connexion.php');
                }else {
                    $erreur = '<p class="text-danger">Veuillez saisir un mot de passe.</p>';
                }
            }else {
                $erreur = '<p class="text-danger">Veuillez saisir une adresse mail.</p>';
            }
        }else {
            $erreur = '<p class="text-danger">Vous n\'avez pas de prénom ?</p>';
        }
    }

?>

<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <p><b>Enregistrez vous<br> ou<br> connectez vous</b></p>
  </div>

  <div class="card text-center">
    <div class="card-body register-card-body">
      <p class="login-box-msg">Vous êtes nouveau ?</p>

      <?php echo $erreur; ?>

        <form method="post">
            <div class="input-group mb-3">
                <input type="text" name="prenom" class="form-control" placeholder="prenom">
                <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-user"></span>
                </div>
                </div>
            </div>
            <div class="input-group mb-3">
                <input type="email" name="email" class="form-control" placeholder="Email">
                <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-envelope"></span>
                </div>
                </div>
            </div>
            <div class="input-group mb-3">
                <input type="password" name="mdp" class="form-control" placeholder="mot de passe">
                <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-lock"></span>
                </div>
                </div>
            </div>
            <div class="row">
                <div class="col-5">
                <button class="btn btn-primary btn-block"><a href="connexion.php" class="text-white">Se connecter</a></button>
                </div>
                <div class="col-5 offset-2">
                <button type="submit" name="submit" class="btn btn-primary btn-block">S'enregistrer</button>
                </div>
            </div>
        </form>
    </div>
    <!-- /.form-box -->
  </div><!-- /.card -->
</div>
<!-- /.register-box -->

<?php include 'footer.php'; ?>

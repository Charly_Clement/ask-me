<?php

include '../connexion-bdd.php';

	try {
		$articles = $pdo->prepare("SELECT * FROM article ORDER BY date_parution");
		$articles->execute();
		$articles = $articles->fetchAll();
	}
	catch (PDOException $e) {
		echo 'Error: '.$e->getMessage();
	}

	$request = $pdo->prepare("SELECT * FROM commentaire");
	$request->execute();
	$nb = $request -> rowcount();

	$recup_logout = isset($_GET['logout']) && !empty($_GET['logout']) ? $_GET['logout'] :'';

        if ($recup_logout == 'ok'){
            session_unset();
            header('Location: ../');
        }

?>
<!DOCTYPE html>
<html lang="fr">

<head>
	<meta charset="utf-8">
	<title>ASK ME</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!-- Main Style -->
	<link rel="stylesheet" href="style.css">
	<!-- Skins -->
	<link rel="stylesheet" href="css/skins/skins.css">
	<!-- Responsive Style -->
	<link rel="stylesheet" href="css/responsive.css">
	<!-- Dark Style -->
	<link rel="stylesheet" href="css/dark.css">
	<!-- Favicons -->
	<link rel="shortcut icon" href="images/favicon.png">
</head>

<body>
	<div class="breadcrumbs">
		<section class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>ACCUEIL</h1>
				</div>
				<div class="col-md-10">
					<div class="crumbs">
						<a href="#">Home</a>
					</div>
				</div>
				<div class="col-md-2">
					<div class="crumbs">
						<a href="?logout=ok">Se déconnecter</a>
					</div>
				</div>
			</div><!-- End row -->
		</section><!-- End container -->
	</div><!-- End breadcrumbs -->
	
	<section class="container main-content">
		<div class="row">
			<div class="col-md-12">
				<?php 	foreach ($articles as $article) { ?>
					<article class="post clearfix">
							<div class="post-inner">
								<?php echo '<div class="post-img"><a href="single_post.php?id='.$article['id_article'].'"><img src="'.$article['photo'].'" alt=""></a></div>'; ?>
							<?php echo '<h2 class="post-title"><span class="post-type"><i class="icon-picture"></i></span><a href="single_post.php?id='.$article['id_article'].'">'. $article['titre'].'</a></h2>' ?>
							<div class="post-meta">
								<span class="meta-date"><i class="icon-time"></i><?php echo $article['date_parution']; ?></span>
								<?php echo '<span class="meta-comment"><i class="icon-comments-alt"></i><a href="single_post.php?='.$article['id_article'].'">'.$nb.' commentaire(s)</a></span>'?>
							</div>
								<div class="post-content">
									<p><?php echo $article['descrip']; ?></p>
									<?php echo '<a href="single_post.php?id='.$article['id_article'].'" class="post-read-more button color small">Continuer à lire</a>' ?>
								</div>
							</div>
					</article>
				<?php } ?>
			</div><!-- End main -->
		</div><!-- End row -->
	</section><!-- End container -->
	
	
	<footer id="footer-bottom">
		<section class="container">
			<div class="copyrights f_left">Copyright 2020 Enssop | <a href="#">d3V w3b</a></div>
			<div class="social_icons f_right">
				<ul>
					<li class="twitter"><a original-title="Twitter" class="tooltip-n" href="#"><i class="social_icon-twitter font17"></i></a></li>
					<li class="facebook"><a original-title="Facebook" class="tooltip-n" href="#"><i class="social_icon-facebook font17"></i></a></li>
					<li class="gplus"><a original-title="Google plus" class="tooltip-n" href="#"><i class="social_icon-gplus font17"></i></a></li>
					<li class="youtube"><a original-title="Youtube" class="tooltip-n" href="#"><i class="social_icon-youtube font17"></i></a></li>
					<li class="skype"><a original-title="Skype" class="tooltip-n" href="skype:#?call"><i class="social_icon-skype font17"></i></a></li>
					<li class="flickr"><a original-title="Flickr" class="tooltip-n" href="#"><i class="social_icon-flickr font17"></i></a></li>
					<li class="rss"><a original-title="Rss" class="tooltip-n" href="#"><i class="social_icon-rss font17"></i></a></li>
				</ul>
			</div><!-- End social_icons -->
		</section><!-- End container -->
	</footer><!-- End footer-bottom -->

<div class="go-up"><i class="icon-chevron-up"></i></div>

<!-- js -->
<script src="js/jquery.min.js"></script>
<script src="js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="js/jquery.easing.1.3.min.js"></script>
<script src="js/html5.js"></script>
<script src="js/twitter/jquery.tweet.js"></script>
<script src="js/jflickrfeed.min.js"></script>
<script src="js/jquery.inview.min.js"></script>
<script src="js/jquery.tipsy.js"></script>
<script src="js/tabs.js"></script>
<script src="js/jquery.flexslider.js"></script>
<script src="js/jquery.prettyPhoto.js"></script>
<script src="js/jquery.carouFredSel-6.2.1-packed.js"></script>
<script src="js/jquery.scrollTo.js"></script>
<script src="js/jquery.nav.js"></script>
<script src="js/tags.js"></script>
<script src="js/jquery.bxslider.min.js"></script>
<script src="js/custom.js"></script>
<!-- End js -->

</body>
</html>
<?php

	include '../connexion-bdd.php';

	$titre     = isset($_POST['titre'])     && !empty($_POST['titre'])     ? $_POST['titre']      : '';
	$descrip   = isset($_POST['descrip'])   && !empty($_POST['descrip'])   ? $_POST['descrip']    : '';
	$photo     = isset($_FILES['photo'])    ? "images/".md5(microtime()).$_FILES['photo']['name'] : '';
	$submit    = isset($_POST['submit'])    && !empty($_POST['submit'])    ? $_POST['submit']     : '';
	
	//$filename = 'images/' . md5(microtime()) . $_FILES['photo']['name'];
	
	if ($submit) {
		try {
		$request = $pdo->prepare("INSERT INTO article (photo,titre,date_parution,descrip)
										VALUES (:photo,:titre,datetime(),:descrip)");
		$request->execute(['photo'=>$photo,'titre'=>$titre,'descrip'=>$descrip]);
		move_uploaded_file($_FILES['photo']['tmp_name'],$photo);
		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
	}

    ?>
<!DOCTYPE html>
<html lang="fr">

<head>
	<meta charset="utf-8">
	<title>ASK ME</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!-- Main Style -->
	<link rel="stylesheet" href="style.css">
	<!-- Skins -->
	<link rel="stylesheet" href="css/skins/skins.css">
	<!-- Responsive Style -->
	<link rel="stylesheet" href="css/responsive.css">
	<!-- Dark Style -->
	<link rel="stylesheet" href="css/dark.css">
	<!-- Favicons -->
	<link rel="shortcut icon" href="images/favicon.png">
</head>

<body>
	<div class="breadcrumbs">
		<section class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>ARTICLES</h1>
				</div>
				<div class="col-md-9">
					<div class="crumbs">
						<a href="index.php">Home</a>
					</div>
				</div>
			</div><!-- End row -->
		</section><!-- End container -->
	</div><!-- End breadcrumbs -->
	
	<div class="container">
		<div class="col-lg-9 ">
			<div class="page-content ask-question offset-3">
				<div class="boxedtitle page-title"><h2>Ajouter ci-dessous votre article</h2></div>
								
				<div class="form-style form-style-3" id="question-submit">
					<form action="" method="post" enctype="multipart/form-data">
						<div class="form-inputs clearfix">
							<p>
								<label class="required">Photo</label>
								<input type="file" name="photo">
							</p>
							<p>
								<label class="required">Titre</label>
								<input type="text" name="titre" id="question-title">
							</p>
							<div class="clearfix"></div>

						</div>
						<div id="form-textarea">
							<p>
								<label class="required">Description<span></span></label>
								<textarea id="question-details" name="descrip" aria-required="true" cols="58" rows="8"></textarea>
							</p>
						</div>
						<div class="form-inputs clearfix">
							<p>
								<input type="submit" name="submit" value="Envoyer" class="submit button small color">
							</p>
						</div>
					</form>
				</div>
			</div><!-- End page-content -->
		</div>
	</div>

	<footer id="footer-bottom">
		<section class="container">
			<div class="copyrights f_left">Copyright <?php echo date('Y') ?> Enssop | <a href="#">d3V w3b</a></div>
			<div class="social_icons f_right">
				<ul>
					<li class="twitter"><a original-title="Twitter" class="tooltip-n" href="#"><i class="social_icon-twitter font17"></i></a></li>
					<li class="facebook"><a original-title="Facebook" class="tooltip-n" href="#"><i class="social_icon-facebook font17"></i></a></li>
					<li class="gplus"><a original-title="Google plus" class="tooltip-n" href="#"><i class="social_icon-gplus font17"></i></a></li>
					<li class="youtube"><a original-title="Youtube" class="tooltip-n" href="#"><i class="social_icon-youtube font17"></i></a></li>
					<li class="skype"><a original-title="Skype" class="tooltip-n" href="skype:#?call"><i class="social_icon-skype font17"></i></a></li>
					<li class="flickr"><a original-title="Flickr" class="tooltip-n" href="#"><i class="social_icon-flickr font17"></i></a></li>
					<li class="rss"><a original-title="Rss" class="tooltip-n" href="#"><i class="social_icon-rss font17"></i></a></li>
				</ul>
			</div><!-- End social_icons -->
		</section><!-- End container -->
	</footer><!-- End footer-bottom -->
</div><!-- End wrap -->

<div class="go-up"><i class="icon-chevron-up"></i></div>

<!-- js -->
<script src="js/jquery.min.js"></script>
<script src="js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="js/jquery.easing.1.3.min.js"></script>
<script src="js/html5.js"></script>
<script src="js/twitter/jquery.tweet.js"></script>
<script src="js/jflickrfeed.min.js"></script>
<script src="js/jquery.inview.min.js"></script>
<script src="js/jquery.tipsy.js"></script>
<script src="js/tabs.js"></script>
<script src="js/jquery.flexslider.js"></script>
<script src="js/jquery.prettyPhoto.js"></script>
<script src="js/jquery.carouFredSel-6.2.1-packed.js"></script>
<script src="js/jquery.scrollTo.js"></script>
<script src="js/jquery.nav.js"></script>
<script src="js/tags.js"></script>
<script src="js/jquery.bxslider.min.js"></script>
<script src="js/custom.js"></script>
<!-- End js -->

</body>
</html>